//var Vue = require('vue/dist/vue.common.js');
import Vue from 'vue/dist/vue.js';
import sayHello from './say_hello';

const app = new Vue({
    el: '#app',
    data: {
        message : sayHello('test')
    }
});
