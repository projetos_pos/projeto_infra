//var vue = required('vue');

//#! /usr/bin/env node

var express = require('express');
var app = express();

//direciona o tráfego de / para a pasta public
app.use('/', express.static('public'));

app.listen(process.env.PORT || 3000, function(){
    console.log('Example listen port 3000');
});